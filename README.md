Hashcat Tutorial
================

Excellent general [resource](https://null-byte.wonderhowto.com/).

And [their
take](https://null-byte.wonderhowto.com/how-to/hack-like-pro-crack-passwords-part-3-using-hashcat-0156543/).

Taken from [this](https://resources.infosecinstitute.com/hashcat-tutorial-beginners/) article.
Another good

[resource](https://laconicwolf.com/2018/09/29/hashcat-tutorial-the-basics-of-cracking-passwords-with-hashcat/).

Hashes generated via:

``` sh
echo -n "Password" | md5sum | tr -d " -" >> target_hashes.txt
echo -n "HELLO" | md5sum | tr -d " -" >> target_hashes.txt
echo -n "MYSECRET" | md5sum | tr -d " -" >> target_hashes.txt
echo -n "Test1234" | md5sum | tr -d " -" >> target_hashes.txt
echo -n "P455w0rd" | md5sum | tr -d " -" >> target_hashes.txt
echo -n "GuessMe" | md5sum | tr -d " -" >> target_hashes.txt
echo -n "S3CuReP455Word" | md5sum | tr -d " -" >> target_hashes.txt
```

Using hashcat to do a dictionary attack with the following:

``` sh
hashcat -m 0 -a 0 -o cracked.txt target_hashes.txt /usr/share/wordlists/rockyou.txt
```

This attacks md5 using a dictionary attack, stores the results in cracked.txt,
grabs the hashes from target_hashes.txt, and uses the rockyou.txt file as the
wordlist.

Using rules takes way longer, but does better:

``` sh
hashcat -a 0 -m 0 -o cracked.txt target_hashes.txt /usr/share/wordlists/rockyou.txt -r ~/git/hashcat/rules/d3ad0ne.rule -O
```
